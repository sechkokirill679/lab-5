#include <iostream>

//вариант 2

using namespace std;


int inputArraySize();
void initializeArray(int* , int);
void printArray(const int*, int);
void sortArray(int* , int);
void findElementsOnOddPositions(const int*, int, int*&, int&);
void removeElementsFromArray(int*&, int&, const int*, int);


int main() {
    int size = inputArraySize();

    int* array = new int[size];

    initializeArray(array, size);

    cout << "Исходный массив: ";
    printArray(array, size);

    sortArray(array, size);

    cout << "Отсортированный массив: ";
    printArray(array, size);

    int* newArray = nullptr;
    int newSize = 0;

    findElementsOnOddPositions(array, size, newArray, newSize);

    removeElementsFromArray(array, size, newArray, newSize);

   
    cout << "Новый массив: ";
    printArray(newArray, newSize);

    cout << "Исходный массив после удаления и сортировки: ";
    printArray(array, size);

    delete[] array;
    delete[] newArray;
}
int inputArraySize() {
    int size;
    cout << "Введите размер массива: ";
    cin >> size;
    return size;
}


void initializeArray(int* array, int size) {
    cout << "Введите элементы массива: ";
    for (int i = 0; i < size; i++) {
        cin >> array[i];
    }
}

void printArray(const int* array, int size) {
    for (int i = 0; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << endl;
}

void sortArray(int* array, int size) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (array[j] > array[j + 1]) {
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}
void findElementsOnOddPositions(const int* array, int size, int*& newArray, int& newSize) {
    newSize = 0;

 
    for (int i = 1; i < size; i += 2) {
        if (array[i] % 2 == 0) {
            newSize++;
        }
    }

   
    newArray = new int[newSize];
    int newIndex = 0;


    for (int i = 1; i < size; i += 2) {
        if (array[i] % 2 == 0) {
            newArray[newIndex++] = array[i];
        }
    }
}

void removeElementsFromArray(int*& array, int& size, const int* newArray, int newSize) {
    int remainingSize = size - newSize;
    int* tempArray = new int[remainingSize];
    int k = 0;

    for (int i = 0; i < size; i++) {
        if ((i % 2 == 0) || (i % 2 == 1 && array[i] % 2 != 0)) {
            tempArray[k++] = array[i];
        }
    }


    delete[] array;
    array = tempArray;
    size = remainingSize;
}
